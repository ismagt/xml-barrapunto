#!/usr/bin/python

#
# Simple XML parser for JokesXML
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2009
#
# Just prints the jokes in a JokesXML file

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

def normalize_whitespace(text):
    "Remove redundant whitespace from a string"
    return string.join(string.split(text), ' ')

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True
            
    def endElement (self, name):
        if name == 'item':
            self.inItem = False
        elif self.inItem:
            if name == 'title':
                line = "Titulo: " + self.theContent + "."
                # To avoid Unicode trouble
                print(line)
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                line = "Enlace: " + self.theContent + "."
                print (line)
                self.inContent = False
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

            
# --- Main prog

if len(sys.argv)<2:
    print("Usage: python xml-parser-jokes.py <document>")
    print (" <document>: file name of the document to parse")
    sys.exit(1)
    
# Load parser and driver

BarraParser = make_parser()
BarraHandler = CounterHandler()
BarraParser.setContentHandler(BarraHandler)

# Ready, set, go!

xmlFile = open(sys.argv[1],"r")
BarraParser.parse(xmlFile)

print ("Parse complete")
