#!/usr/bin/python3
# -*- coding: utf-8 -*-


from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
from urllib import request

class myContentHandler(ContentHandler):

    def __init__ (self):
        self.archivo = open("contents.html", "w")

        self.inItem = False
        self.inContent = False
        self.theContent = ""
        self.line = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True

    def endElement (self, name):
        if name == 'item':
            self.inItem = False
        elif self.inItem:
            if name == 'title':
                self.line = "<li>Titulo: " + self.theContent + ".</li>"
                # To avoid Unicode trouble
                print(self.line)
                self.archivo.write(self.line)
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                self.link = self.theContent
                links = "<p>Enlace: <a href='" + self.link + "'>" + self.link + "</a></p>\n"
                print(links)
                self.archivo.write(links)
                self.inContent = False
                self.theContent = ""
                self.link = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# Load parser and driver
theParser = make_parser()
theHandler = myContentHandler()
theParser.setContentHandler(theHandler)

# Ready, set, go!2  --> con la pagina web original de barrapunto
url = "http://barrapunto.com/index.rss"
xmlStream = request.urlopen(url)
theParser.parse(xmlStream)
